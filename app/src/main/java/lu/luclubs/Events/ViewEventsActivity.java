package lu.luclubs.Events;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.widget.ListView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import lu.luclubs.R;
import lu.luclubs.clublist.ClubInfo;
import lu.luclubs.clublist.ClubListAdapter;

public class ViewEventsActivity extends AppCompatActivity {

    private  ArrayList<EventInfo> clubInfoArrayList = null;
    ListView clubListView = null;
    EventListAdapter clubListAdapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_events);

        clubListView = (ListView) findViewById(R.id.listview_view_event);
        clubInfoArrayList = new ArrayList<>();
        clubListAdapter = new EventListAdapter(ViewEventsActivity.this, clubInfoArrayList);
        clubListView.setAdapter(clubListAdapter);
    }
}
