package lu.luclubs.Events;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.clublist.ClubInfo;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;

/**
 * Created by Siyam on 11-Sep-16.
 */
public class EventListAdapter extends BaseAdapter implements HTTPRequestIDs,HttpListener {
    Activity activity = null;
    ArrayList<EventInfo> clubInfoArrayList = null;
    int positionToRemove = 0;

    public EventListAdapter(Activity activity, ArrayList<EventInfo> crLists) {
        this.activity = activity;
        this.clubInfoArrayList = crLists;
        getCrListRequest();
    }

    @Override
    public int getCount() {

//        Log.i("getView","getcount"+clubInfoArrayList.size() + "");
        return clubInfoArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        Log.i("getView", "list size:" + clubInfoArrayList.size());
        if (convertView == null) {
            convertView = LayoutInflater.from(activity).inflate(R.layout.view_event_list_item, parent, false);
        }

//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
//                builder1.setMessage("Remove this student from class representative List?");
//                builder1.setCancelable(true);
//                builder1.setPositiveButton("Confirm",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                StaticUserInterface.showProgressDialog(activity,"Removing From Class Representative List..");
//                                positionToRemove = position;
//                                sendremoveCrRequest(position);
//
//                            }
//                        });
//                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog alert11 = builder1.create();
//                alert11.show();
//                alert11.setCanceledOnTouchOutside(false);
//                return false;
//            }
//        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(activity);
                dialog.setContentView(R.layout.event_description);
                ((TextView)dialog.findViewById(R.id.textview_event_description)).setText("\n" +
                        ((EventInfo) clubInfoArrayList.get(position)).getDescription() + "\n\n"
                );
                dialog.setTitle(((EventInfo) clubInfoArrayList.get(position)).getTitle());
                dialog.show();
            }
        });
        TextView title = (TextView) convertView.findViewById(R.id.event_item_title);
        TextView location = (TextView) convertView.findViewById(R.id.event_item_location);
        TextView dateTime = (TextView) convertView.findViewById(R.id.event_item_datetime);
        title.setText("Title: " + ((EventInfo) clubInfoArrayList.get(position)).getTitle());
        location.setText("Location: " + ((EventInfo) clubInfoArrayList.get(position)).getLocation());
        dateTime.setText("Date-Time: " + ((EventInfo) clubInfoArrayList.get(position)).getDatetime());

        return convertView;
    }
//    private void sendremoveCrRequest(int position) {
//
//        HttpRequest addNewCr =  new HttpRequest(activity.getString(R.string.remove_cr) + clubInfoArrayList.get(position).getClub_id(),
//                REQUEST_REMOVE_CR,ClubListAdapter.this);
//        addNewCr.setParams(new KeyValue("access_token", SharedPreferencesReadAndWrite.readItemFromSharedPreferences(activity,"access_token")));
//        addNewCr.execute("post");
//    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {
        JSONObject jsonObjectResponse;
        if (respondId == REQUEST_GET_ALL_EVENTS) {
            try {
                Log.i("mmrs", jsonRespond);
                StaticUserInterface.dismissProgressDialog();
                jsonObjectResponse = new JSONObject(jsonRespond);
                JSONArray clubList = jsonObjectResponse.getJSONArray("events");
                clubInfoArrayList.clear();
                for (int i = 0; i < clubList.length(); i++) {
                    JSONObject club_info = clubList.getJSONObject(i);
                    EventInfo clubInfo = new EventInfo();
                    clubInfo.setId(club_info.getString("id"));
                    clubInfo.setTitle(club_info.getString("title"));
                    clubInfo.setLocation(club_info.getString("location"));
                    clubInfo.setDatetime(club_info.getString("datetime"));
                    clubInfo.setDescription(club_info.getString("description"));
                    clubInfoArrayList.add(clubInfo);
//                        Log.i("mmrs",clubInfo.getName());
//                        Log.i("mmrs",clubInfo.getSession());
//                        Log.i("mmrs",clubInfo.getClubName());
                }
                notifyDataSetChanged();

            } catch (Exception e) {

                StaticUserInterface.alartDialog(activity, e.toString());

            }

        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {
        if (respondId == REQUEST_CLUB_LIST) {

            StaticUserInterface.dismissProgressDialog();
            StaticUserInterface.alartDialog(activity, e.toString());
        }
    }

    @Override
    public Context getContext() {
        return activity;
    }

    //request to get current cr list
    public void getCrListRequest() {

        HttpRequest httpRequest = new HttpRequest(StaticData.getSocket() + "/api/events",
                REQUEST_GET_ALL_EVENTS, this);
        httpRequest.execute("get");
    }

}
