package lu.luclubs.clublist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import lu.luclubs.AboutUsActivity;
import lu.luclubs.Events.ViewEventsActivity;
import lu.luclubs.LoginActivity;
import lu.luclubs.R;
import lu.luclubs.SharedPreferencesReadAndWrite;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.UpdateProfileActivity;
import lu.luclubs.administration.AdministrationOptionsActivity;

public class ClubListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private  ArrayList<ClubInfo> clubInfoArrayList = null;
    ListView clubListView = null;
    ClubListAdapter clubListAdapter = null;
    CardView administration,newsUpdate;
    CircleImageView navHeaderImageView;
    boolean isFirstTime = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Refreshing Club List", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                clubListAdapter.getCrListRequest();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headView = navigationView.getHeaderView(0);
        navHeaderImageView = (CircleImageView) headView.findViewById(R.id.nav_header_imageView);
//        navHeaderImageView.setImageResource(R.drawable.club_news);

        headView.findViewById(R.id.navigation_item_update_my_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClubListActivity.this,UpdateProfileActivity.class));
            }
        });
        headView.findViewById(R.id.navigation_logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                SharedPreferencesReadAndWrite.clearSharedPreferencesData(ClubListActivity.this);
                startActivity(new Intent(ClubListActivity.this, LoginActivity.class));
            }
        });
        headView.findViewById(R.id.navigation_about_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ClubListActivity.this, AboutUsActivity.class));
            }
        });
        Picasso.with(ClubListActivity.this)
                .load(StaticUserInfo.getPhoto())
                .resize(150, 150)
                .centerCrop()
                .into(navHeaderImageView);

        administration = (CardView) findViewById(R.id.clublist_cardview_administration);
        newsUpdate = (CardView) findViewById(R.id.clublist_cardview_newsupdate);
//        if(!StaticUserInfo.getRole().equals("admin"))
//            administration.setVisibility(View.GONE);
        clubListView = (ListView) findViewById(R.id.clublist_listview);
        clubInfoArrayList = new ArrayList<>();
        clubListAdapter = new ClubListAdapter(ClubListActivity.this, clubInfoArrayList);
        clubListView.setAdapter(clubListAdapter);
//        StaticUserInterface.showProgressDialog(CRListActivity.this,"Retrieving Class Representative List..");
//        getCrListRequest();


        findViewById(R.id.relative_layout_administration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClubListActivity.this, AdministrationOptionsActivity.class));
            }
        });
        findViewById(R.id.relative_layout_view_events).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClubListActivity.this, ViewEventsActivity.class));
            }
        });

        if(!StaticUserInfo.getRole().equals("admin")){
            findViewById(R.id.relative_layout_administration).setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.club_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
