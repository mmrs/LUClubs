//package lu.luclubs.clublist;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.android.volley.VolleyError;
//import com.infancyit.mmcnews.R;
//import com.infancyit.mmcnews.SharedPreferencesReadAndWrite;
//import com.infancyit.mmcnews.StaticUserInterface;
//import com.infancyit.mmcnews.httprequest.HTTPRequestIDs;
//import com.infancyit.mmcnews.httprequest.HttpListener;
//import com.infancyit.mmcnews.httprequest.HttpRequest;
//import com.infancyit.mmcnews.httprequest.KeyValue;
//import com.squareup.picasso.Picasso;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//
//public class CRListActivity extends AppCompatActivity implements HttpListener,HTTPRequestIDs{
//
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_crlist);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        final FloatingActionButton addNewCr = (FloatingActionButton) findViewById(R.id.fab);
//        addNewCr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                addNewCrDialog = new Dialog(CRListActivity.this);
//                addNewCrDialog.getWindow();
//                addNewCrDialog.setContentView(R.layout.add_new_cr_dialog);
//                addNewCrDialog.show();
//                Button getUserInfoButton = (Button) addNewCrDialog.findViewById(R.id.get_user_info);
//                phoneNo = (EditText) addNewCrDialog.findViewById(R.id.user_phone_no);
//                getUserInfoprogressBar = (ProgressBar)addNewCrDialog.findViewById(R.id.progressbar_user_info);
//                layoutGetInfo = addNewCrDialog.findViewById(R.id.get_mobile_no_linear_layout);
//                layoutShowInfo = addNewCrDialog.findViewById(R.id.student_inf0_relative_layout);
//                studentInfoName = (TextView) addNewCrDialog.findViewById(R.id.user_name);
//                studentInfoSession = (TextView) addNewCrDialog.findViewById(R.id.user_session);
//                studentInfoRoll = (TextView) addNewCrDialog.findViewById(R.id.user_roll);
//                studentInfoPhoto = (ImageView) addNewCrDialog.findViewById(R.id.user_image);
//                confirmCrButton = (Button) addNewCrDialog.findViewById(R.id.confirm_add_cr);
//
//                getUserInfoButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        layoutGetInfo.setVisibility(View.GONE);
//                        getUserInfoprogressBar.setVisibility(View.VISIBLE);
//                        getUserInfoRequest();
//                    }
//                });
//
//                confirmCrButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StaticUserInterface.showProgressDialog(CRListActivity.this,"Adding Student To Class representative List..");
//                        sendAddNewCrRequest();
//                    }
//                });
//
//            }
//        });
//
//
//
//    }
////request to get current cr list
//    public void getCrListRequest(){
//
//        httpRequest = new HttpRequest(getString(R.string.get_cr_list) + "?access_token="+
//                SharedPreferencesReadAndWrite.readItemFromSharedPreferences(CRListActivity.this,"access_token"),
//                REQUEST_GET_CR_LIST,this);
//        httpRequest.execute("get");
//    }
////request to add new cr to the list
//    private void sendAddNewCrRequest() {
//        httpRequest =  new HttpRequest(getString(R.string.add_cr) + userInfo.getClub_id(),
//                REQUEST_ADD_NEW_CR,CRListActivity.this);
//        httpRequest.setParams(new KeyValue("access_token",SharedPreferencesReadAndWrite.readItemFromSharedPreferences(CRListActivity.this,"access_token")));
//        httpRequest.execute("post");
//    }
// //request to get particular user information
//    private void getUserInfoRequest(){
//
//        httpRequest = new HttpRequest(getString(R.string.get_user_info_by_phone_no) + "?access_token="+
//                SharedPreferencesReadAndWrite.readItemFromSharedPreferences(CRListActivity.this,"access_token") + "&key=" + phoneNo.getText().toString(),
//                REQUEST_GET_USER_INFO_BY_PHONENO,this);
//        httpRequest.execute("get");
//    }
//    @Override
//    public void respond(String jsonRespond, int respondId, HttpRequest parent) {
//
//        JSONObject jsonObjectResponse;
//        if(respondId == REQUEST_GET_CR_LIST){
//            try{
//                Log.i("mmrs",jsonRespond);
//                StaticUserInterface.dismissProgressDialog();
//                jsonObjectResponse = new JSONObject(jsonRespond);
//                if(jsonObjectResponse.getString("status").equals("200")){
//
//                    JSONArray crList = jsonObjectResponse.getJSONObject("message").getJSONArray("data");
//
//                    clubInfoArrayList.clear();
//
//                    for(int i=0;i<crList.length();i++){
//
//                        JSONObject user_info = crList.getJSONObject(i).getJSONObject("user_info");
//                        ClubInfo clubInfo = new ClubInfo();
//                        clubInfo.setName(user_info.getString("name"));
//                        clubInfo.setClubName(user_info.getString("clubName"));
//                        clubInfo.setSession(user_info.getString("session"));
//                        clubInfo.setClub_id(user_info.getString("club_id"));
//                        clubInfo.setClubPhotoUrl(user_info.getString("clubPhotoUrl"));
//                        clubInfoArrayList.add(clubInfo);
////                        Log.i("mmrs",clubInfo.getName());
////                        Log.i("mmrs",clubInfo.getSession());
////                        Log.i("mmrs",clubInfo.getClubName());
//                    }
//                    crListAdapter.notifyDataSetChanged();
//                }
//            }catch (Exception e){
//
//                StaticUserInterface.alartDialog(CRListActivity.this,e.toString());
//
//            }
//
//        }
//        else if(respondId == REQUEST_ADD_NEW_CR){
////            Log.i("mmrs",jsonRespond);
//
//            try{
//                StaticUserInterface.dismissProgressDialog();
//                jsonObjectResponse = new JSONObject(jsonRespond);
//                if(jsonObjectResponse.getString("status").equals("200")){
//                    addNewCrDialog.dismiss();
//                    //udating CR list
//                    getCrListRequest();
//
//                }
//                StaticUserInterface.makeToast(CRListActivity.this,jsonObjectResponse.getString("message") + ".");
//                }
//            catch (Exception e){
//                StaticUserInterface.alartDialog(CRListActivity.this,e.toString());
//            }
//        }
//
//        else if(respondId == REQUEST_GET_USER_INFO_BY_PHONENO){
//
//            getUserInfoprogressBar.setVisibility(View.GONE);
//
//            try{
//                jsonObjectResponse = new JSONObject(jsonRespond);
//                if(jsonObjectResponse.getString("status").equals("200")){
//
//                    layoutShowInfo.setVisibility(View.VISIBLE);
//                    JSONObject user_info = jsonObjectResponse.getJSONObject("message").getJSONObject("user_info");
//                    userInfo = new ClubInfo();
//                    userInfo.setName(user_info.getString("name"));
//                    userInfo.setClubName(user_info.getString("clubName"));
//                    userInfo.setSession(user_info.getString("session"));
//                    userInfo.setClub_id(user_info.getString("club_id"));
//                    userInfo.setClubPhotoUrl(user_info.getString("clubPhotoUrl"));
//
//                    studentInfoName.setText("Name: "+userInfo.getName());
//                    studentInfoSession.setText("Session: "+ userInfo.getSession());
//                    studentInfoRoll.setText("Roll: " + userInfo.getClubName());
//
//                    Picasso.with(CRListActivity.this)
//                            .load(userInfo.getClubPhotoUrl())
//                            .resize(150, 150)
//                            .centerCrop()
//                            .into(studentInfoPhoto);
//                    Log.i("mmrs",userInfo.getClubPhotoUrl());
//                }
//                else {
//                    layoutGetInfo.setVisibility(View.VISIBLE);
//                    StaticUserInterface.makeToast(CRListActivity.this,jsonObjectResponse.getString("message"));
//                }
//            }
//            catch (Exception e){
//                addNewCrDialog.dismiss();
//                StaticUserInterface.alartDialog(CRListActivity.this,e.toString());
//            }
//
//
//        }
//
//    }
//
//    @Override
//    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {
//        if (respondId == REQUEST_GET_CR_LIST || respondId == REQUEST_ADD_NEW_CR) {
//
//            StaticUserInterface.dismissProgressDialog();
//            StaticUserInterface.alartDialog(CRListActivity.this,e.toString());
//
//        }
//        else if( respondId == REQUEST_GET_USER_INFO_BY_PHONENO){
//            layoutGetInfo.setVisibility(View.VISIBLE);
//            StaticUserInterface.alartDialog(CRListActivity.this,e.toString());
//            addNewCrDialog.dismiss();
//        }
//
//    }
//
//    @Override
//    public Context getContext() {
//        return CRListActivity.this;
//    }
//}
