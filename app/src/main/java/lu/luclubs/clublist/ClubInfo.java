package lu.luclubs.clublist;

/**
 * Created by Siyam on 18-Aug-16.
 */
public class ClubInfo {

    String clubName;
    String club_id;
    String clubPhotoUrl;
    String userStatus = "JOIN CLUB";
    String userRole = "";
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getClubPhotoUrl() {return clubPhotoUrl;}
    public void setClubPhotoUrl(String clubPhotoUrl) {
        this.clubPhotoUrl = clubPhotoUrl;
    }

    public String getClubName() {
        return clubName;
    }

    public String getClub_id() {
        return club_id;
    }

    public void setClub_id(String club_id) {
        this.club_id = club_id;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

}
