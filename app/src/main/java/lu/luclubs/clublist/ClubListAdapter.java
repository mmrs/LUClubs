package lu.luclubs.clublist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import lu.luclubs.Events.EventInfo;
import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;
import lu.luclubs.inclub.InClubActivity;

/**
 * Created by Siyam on 18-Aug-16.
 */
public class ClubListAdapter extends BaseAdapter implements HTTPRequestIDs,HttpListener {

    Activity activity = null;
    ArrayList<ClubInfo> clubInfoArrayList = null;
    int positionToRemove = 0;

    public ClubListAdapter(Activity activity, ArrayList<ClubInfo> crLists) {
        this.activity = activity;
        this.clubInfoArrayList = crLists;
        getCrListRequest();
    }

    @Override
    public int getCount() {

//        Log.i("getView","getcount"+clubInfoArrayList.size() + "");
        return clubInfoArrayList.size();

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        Log.i("getView","list size:" +clubInfoArrayList.size());
        if(convertView == null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.club_list_item,parent,false);
        }

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(!StaticUserInfo.getRole().equals("admin")){
                    return false;
                }

                AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
                builder1.setMessage("Sure To Remove this Club? All club related info and sections will be deleted permanently.");
                builder1.setCancelable(true);
                builder1.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                positionToRemove = position;
                                sendremoveClubRequest(position);

                            }
                        });
                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = builder1.create();
                alert11.show();
                alert11.setCanceledOnTouchOutside(false);
                return false;
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!((ClubInfo)clubInfoArrayList.get(position)).getUserStatus().equals("approved")){
                    Dialog dialog = new Dialog(activity);
                    dialog.setContentView(R.layout.event_description);
                    ((TextView)dialog.findViewById(R.id.textview_event_description)).setText("\n" +
                            ((ClubInfo) clubInfoArrayList.get(position)).getDescription() + "\n\n"
                    );
                    dialog.setTitle("Club Info ");
                    dialog.show();
                    return;
                }

                StaticData.setCurrentClubId(((ClubInfo)clubInfoArrayList.get(position)).getClub_id());
                StaticData.setClubRole(((ClubInfo)clubInfoArrayList.get(position)).getUserRole());
                activity.startActivity(new Intent(activity, InClubActivity.class));

            }
        });
////            Log.i("getView",position + ": " + clubInfoArrayList.get(position).getName());
            TextView name = (TextView) convertView.findViewById(R.id.club_name);
            ImageView photo = (ImageView) convertView.findViewById(R.id.club_item_logo);
            Button joinClubButton = (Button) convertView.findViewById(R.id.join_club);
            joinClubButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendJoinClubRequest(position);
            }
        });

        name.setText(((ClubInfo)clubInfoArrayList.get(position)).getClubName());
        joinClubButton.setText(((ClubInfo)clubInfoArrayList.get(position)).getUserStatus());

        if(joinClubButton.getText().toString().equals("approved") || joinClubButton.getText().toString().equals("pending")){
            joinClubButton.setClickable(false);
        }

        Picasso.with(activity)
                .load(StaticData.getSocket()+((ClubInfo) clubInfoArrayList.get(position)).getClubPhotoUrl())
                .resize(150, 150)
                .centerCrop()
                .into(photo);
        return convertView;

    }

    private void sendJoinClubRequest(int position) {

        HttpRequest addNewCr =  new HttpRequest(StaticData.getSocket()+"/api/clubs/join",
                REQUEST_JOIN_CLUB,ClubListAdapter.this);
        addNewCr.setParams(
                new KeyValue("club_id",clubInfoArrayList.get(position).getClub_id()),
                new KeyValue("user_id",StaticUserInfo.getId())
        );
        addNewCr.execute("post");


    }

    private void sendremoveClubRequest(int position) {

        StaticUserInterface.showProgressDialog(activity,"Sending Request to Join this Club..");
        HttpRequest addNewCr =  new HttpRequest(StaticData.getSocket()+"/api/clubs/delete/" + clubInfoArrayList.get(position).getClub_id(),
                REQUEST_DELETE_CLUB,ClubListAdapter.this);
        addNewCr.execute("get");
    }


    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {
        JSONObject jsonObjectResponse;
        StaticUserInterface.dismissProgressDialog();
        if(respondId == REQUEST_CLUB_LIST){
            try{
                Log.i("mmrs",jsonRespond);
                jsonObjectResponse = new JSONObject(jsonRespond);
                    JSONArray clubList = jsonObjectResponse.getJSONArray("clubs");
                    clubInfoArrayList.clear();
                    notifyDataSetChanged();
                for(int i=0;i<clubList.length();i++){
                        JSONObject club_info = clubList.getJSONObject(i);
                        ClubInfo clubInfo = new ClubInfo();
                        clubInfo.setClubName(club_info.getString("name"));
                        clubInfo.setClub_id(club_info.getString("id"));
                        clubInfo.setClubPhotoUrl(club_info.getString("photo_url"));
                        clubInfo.setDescription(club_info.getString("description"));
                    Log.i("mmrs","parsing " + clubInfo.getClubName());
                    if(club_info.has("user_club")){
                        if(!club_info.getString("user_club").equals("null")){
                            Log.i("mmrs","null member of " + clubInfo.getClubName());
                            JSONObject userClub  = club_info.getJSONObject("user_club");
                            clubInfo.setUserRole(userClub.getString("role"));
                            clubInfo.setUserStatus(userClub.getString("status"));
                        }
                    }


                        clubInfoArrayList.add(clubInfo);
//                        Log.i("mmrs",clubInfo.getName());
//                        Log.i("mmrs",clubInfo.getSession());
//                        Log.i("mmrs",clubInfo.getClubName());
                    }
                    notifyDataSetChanged();

            }catch (Exception e){

                StaticUserInterface.alartDialog(activity,e.toString());

            }
        }
        else if(respondId == REQUEST_DELETE_CLUB){
            StaticUserInterface.makeToast(activity,"Club and other info deleted with this club deleted permanently");
            getCrListRequest();
        }

        else if(respondId == REQUEST_JOIN_CLUB){

            StaticUserInterface.makeToast(activity,"Request Received.You can enter the club after admin approval");
            getCrListRequest();
        }
    }
    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();
        if (respondId == REQUEST_CLUB_LIST) {

            StaticUserInterface.dismissProgressDialog();
            StaticUserInterface.alartDialog(activity,e.toString());
        }
        else if(respondId == REQUEST_DELETE_CLUB){

        }
    }

    @Override
    public Context getContext() {
        return activity;
    }

    //request to get current cr list
    public void getCrListRequest(){


        HttpRequest httpRequest = new HttpRequest(StaticData.getSocket() + "/api/clubs/" + StaticUserInfo.getId(),
                REQUEST_CLUB_LIST, this);
        httpRequest.execute("get");
    }
    
}
