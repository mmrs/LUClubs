package lu.luclubs;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import lu.luclubs.clublist.ClubListActivity;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements HttpListener,HTTPRequestIDs{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ((EditText) findViewById(R.id.socket)).setText(SharedPreferencesReadAndWrite.readItemFromSharedPreferences(LoginActivity.this,"socket"));
        ((EditText) findViewById(R.id.login_student_id)).setText(SharedPreferencesReadAndWrite.readItemFromSharedPreferences(LoginActivity.this,"id"));
        ((EditText) findViewById(R.id.login_password)).setText(SharedPreferencesReadAndWrite.readItemFromSharedPreferences(LoginActivity.this,"password"));
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.setSocket("http://" + ((EditText) findViewById(R.id.socket)).getText().toString());
                SharedPreferencesReadAndWrite.writeItemToSharedPreferences(LoginActivity.this,"socket",((EditText) findViewById(R.id.socket)).getText().toString());
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });

        findViewById(R.id.forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticData.setSocket("http://" + ((EditText) findViewById(R.id.socket)).getText().toString());
                SharedPreferencesReadAndWrite.writeItemToSharedPreferences(LoginActivity.this,"socket",((EditText) findViewById(R.id.socket)).getText().toString());
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticData.setSocket("http://" + ((EditText) findViewById(R.id.socket)).getText().toString());
               SharedPreferencesReadAndWrite.writeItemToSharedPreferences(LoginActivity.this,"socket",((EditText) findViewById(R.id.socket)).getText().toString());
                HttpRequest logIn = new HttpRequest(StaticData.getSocket()+"/api/login",REQUEST_LOG_IN,LoginActivity.this);
                logIn.setParams(
                        new KeyValue("student_id",((EditText)findViewById(R.id.login_student_id)).getText().toString()),
                        new KeyValue("password",((EditText)findViewById(R.id.login_password)).getText().toString())
                );
                StaticUserInterface.showProgressDialog(LoginActivity.this,"please Wait..");
                logIn.execute("post");
            }
        });
    }
    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        JSONObject jsonObjectResponse;
        try{
            if(respondId ==REQUEST_LOG_IN){
                StaticUserInterface.dismissProgressDialog();
                jsonObjectResponse = new JSONObject(jsonRespond).getJSONObject("user");
                Log.i("mmrs",jsonRespond);
                Intent intent = new Intent(LoginActivity.this,ClubListActivity.class);
                startActivity(intent);
                StaticUserInfo.setId(jsonObjectResponse.getString("id"));
                StaticUserInfo.setStudent_id(jsonObjectResponse.getString("student_id"));
                StaticUserInfo.setDepartment(jsonObjectResponse.getString("department"));
                StaticUserInfo.setRole(jsonObjectResponse.getString("role"));
                StaticUserInfo.setMobile(jsonObjectResponse.getString("mobile"));
                StaticUserInfo.setPhoto(StaticData.getSocket() + jsonObjectResponse.getString("photo_url"));
                StaticUserInfo.setEmail(jsonObjectResponse.getString("email"));
                StaticUserInterface.makeToast(LoginActivity.this,"Log in Success");
                SharedPreferencesReadAndWrite.writeItemToSharedPreferences(LoginActivity.this,"id",((EditText)findViewById(R.id.login_student_id)).getText().toString());
                SharedPreferencesReadAndWrite.writeItemToSharedPreferences(LoginActivity.this,"password",((EditText)findViewById(R.id.login_password)).getText().toString());
                finish();
            }
        }
        catch (Exception e){
            StaticUserInterface.alartDialog(LoginActivity.this,e.toString());
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        if(respondId==REQUEST_LOG_IN){
            StaticUserInterface.dismissProgressDialog();
            StaticUserInterface.alartDialog(LoginActivity.this,e.toString());
        }

    }

    @Override
    public Context getContext() {
        return LoginActivity.this;
    }
}




