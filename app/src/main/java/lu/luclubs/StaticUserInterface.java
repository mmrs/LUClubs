package lu.luclubs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by Siyam on 11-Aug-16.
 */
public class StaticUserInterface {

    static AlertDialog.Builder builder1;
    static ProgressDialog progressDialog;

    public static void makeToast(Context context, String msg){

        Toast.makeText(context,msg, Toast.LENGTH_SHORT).show();
    }

    public static void alartDialog(final Context context, String msg) {
        builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setCancelable(true);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
        alert11.setCanceledOnTouchOutside(false);
    }

    public static void showProgressDialog(Context context,String msg){

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);

    }

    public static void dismissProgressDialog(){

        if(progressDialog!=null)
        progressDialog.dismiss();
    }
}
