package lu.luclubs.inclub;

/**
 * Created by Moshiur on 27-Sep-16.
 */

public class MessageInfo {

    String messageBody,messageSenderName,messageSenderPhoto,dateTime,senderId;

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageSenderName() {
        return messageSenderName;
    }

    public void setMessageSenderName(String messageSenderName) {
        this.messageSenderName = messageSenderName;
    }

    public String getMessageSenderPhoto() {
        return messageSenderPhoto;
    }

    public void setMessageSenderPhoto(String messageSenderPhoto) {
        this.messageSenderPhoto = messageSenderPhoto;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
