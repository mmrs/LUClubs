package lu.luclubs.inclub;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import lu.luclubs.LoginActivity;
import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

/**
 * Created by Moshiur on 25-Sep-16.
 */

public class PendingRequestAdapter extends BaseAdapter implements HttpListener,HTTPRequestIDs {

    Activity activity;
    ArrayList<MemberInfo> memberInfoArrayList = new ArrayList<>();
    public PendingRequestAdapter(PendingRequestActivity pendingRequestActivity) {
        this.activity = pendingRequestActivity;
        sendListrequest();
        StaticUserInterface.showProgressDialog(activity,"Getting Pending Requests..");

    }

    private void sendListrequest() {
        HttpRequest addNewCr =  new HttpRequest(StaticData.getSocket()+"/api/clubs/requests/" + StaticData.getCurrentClubId(),
                REQUEST_PENDING_REQUEST_LIST,PendingRequestAdapter.this);
        addNewCr.execute("get");
    }

    @Override
    public int getCount() {
        return memberInfoArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.list_item_pending_request,parent,false);
        }
//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
//                builder1.setMessage("Sure To Remove this Club? All club related info and sections will be deleted permanently.");
//                builder1.setCancelable(true);
//                builder1.setPositiveButton("Confirm",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                positionToRemove = position;
//                                sendremoveClubRequest(position);
//
//                            }
//                        });
//                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog alert11 = builder1.create();
//                alert11.show();
//                alert11.setCanceledOnTouchOutside(false);
//                return false;
//            }
//        });
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                StaticData.setCurrentClubId(((ClubInfo)clubInfoArrayList.get(position)).getClub_id());
//                activity.startActivity(new Intent(activity, InClubActivity.class));
//
//            }
//        });
////            Log.i("getView",position + ": " + clubInfoArrayList.get(position).getName());

        ImageView photo = (ImageView) convertView.findViewById(R.id.imageView_pending_request);
        Picasso.with(activity)
                .load(StaticData.getSocket()+((MemberInfo)memberInfoArrayList.get(position)).getPhoto())
                .resize(70, 70)
                .centerCrop()
                .into(photo);
        ((TextView)convertView.findViewById(R.id.name_pending_request)).setText("Name: "+memberInfoArrayList.get(position).getName());
        ((TextView)convertView.findViewById(R.id.dept_pending_request)).setText("Dept.: "+memberInfoArrayList.get(position).getDept());
        ((TextView)convertView.findViewById(R.id.roll__pending_request)).setText("student ID: " + memberInfoArrayList.get(position).getStudentId());
        ((TextView)convertView.findViewById(R.id.batch_pending_request)).setText("Session: " + memberInfoArrayList.get(position).getBatch());
        Button reject = (Button) convertView.findViewById(R.id.reject_pending_request);
        final Button approve = (Button) convertView.findViewById(R.id.approve_pending_request);
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticUserInterface.showProgressDialog(activity,"processing..");
                HttpRequest logIn = new HttpRequest(StaticData.getSocket()+"/api/clubs/processRequest",REQUEST_PROCESS_REQUEST,PendingRequestAdapter.this);
                logIn.setParams(
                        new KeyValue("club_id",StaticData.getCurrentClubId()),
                        new KeyValue("user_id",((MemberInfo)memberInfoArrayList.get(position)).getUserId()),
                        new KeyValue("status","rejected")
                );
                logIn.execute("post");
            }
        });
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticUserInterface.showProgressDialog(activity,"processing..");
                HttpRequest logIn = new HttpRequest(StaticData.getSocket()+"/api/clubs/processRequest",REQUEST_PROCESS_REQUEST,PendingRequestAdapter.this);
                logIn.setParams(
                        new KeyValue("club_id",StaticData.getCurrentClubId()),
                        new KeyValue("user_id",((MemberInfo)memberInfoArrayList.get(position)).getUserId()),
                        new KeyValue("status","approved")
                );
                logIn.execute("post");

            }
        });

        return convertView;
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();
        JSONObject jsonObjectResponse;
        if(respondId == REQUEST_PENDING_REQUEST_LIST){
            try{
                Log.i("mmrs","member list " +jsonRespond);
                jsonObjectResponse = new JSONObject(jsonRespond);
                JSONArray clubList = jsonObjectResponse.getJSONArray("requests");
                memberInfoArrayList.clear();
                for(int i=0;i<clubList.length();i++){
                    JSONObject info = clubList.getJSONObject(i);
                    MemberInfo memberInfo = new MemberInfo();
                    memberInfo.setRole(info.getString("role"));
                    memberInfo.setStatus(info.getString("status"));
                    JSONObject userInfo = info.getJSONObject("user");
                    memberInfo.setUserId(userInfo.getString("id"));
                    memberInfo.setName(userInfo.getString("name"));
                    memberInfo.setStudentId(userInfo.getString("student_id"));
                    memberInfo.setDept(userInfo.getString("department"));
                    memberInfo.setBatch(userInfo.getString("batch"));
                    memberInfo.setPhoto(userInfo.getString("photo_url"));
                    memberInfoArrayList.add(memberInfo);
                }
                notifyDataSetChanged();
            }catch (Exception e){
                Log.i("mmrs","exception in pending request parsing");

            }
        }

        else if(respondId == REQUEST_PROCESS_REQUEST){
            StaticUserInterface.makeToast(activity,"Request Processed");
            sendListrequest();
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();
        Log.i("mmrs","error response in  pending requests");

    }

    @Override
    public Context getContext() {
        return activity ;
    }

}
