package lu.luclubs.inclub;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HTTPResponseIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class ChangePostActivity extends AppCompatActivity implements HTTPRequestIDs,HttpListener{

    AppCompatSpinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        spinner = (AppCompatSpinner) findViewById(R.id.spinner_change_membership);
        // Spinner element
//        Spinner spinner = (Spinner) findViewById(R.id.spinner_departments);

        // Spinner click listener
//        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        final List<String> categories = new ArrayList<String>();
        categories.add("Adviser");
        categories.add("Co-President");
        categories.add("President");
        categories.add("Vice_President");
        categories.add("Secretary");
        categories.add("Joint_Secretary");
        categories.add("Organizer");
        categories.add("Co-Organizer");
        categories.add("Treasurer");
        categories.add("Joint_Treasurer");
        categories.add("Public_Relationship_Officer");
        categories.add("Official_Photographer");
        categories.add("Executive_Member");
        categories.add("admin");
        categories.add("user");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.department_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);



        findViewById(R.id.button_change_membership).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HttpRequest removeMember = new HttpRequest(StaticData.getSocket()+"/api/members/role/"
                        + StaticData.getCurrentClubId()+"/"+StaticData.getCurrentMemberId() + "/" + categories.get(spinner.getSelectedItemPosition())
                        ,REQUEST_CHANGE_MEMBERSHIP,ChangePostActivity.this);

                Log.i("mmrs","debug: "+categories.get(spinner.getSelectedItemPosition()));
                removeMember.execute("get");

            }
        });

        findViewById(R.id.button_remove_from_club).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HttpRequest removeMember = new HttpRequest(StaticData.getSocket()+"/api/members/remove/"
                        + StaticData.getCurrentClubId()+"/"+StaticData.getCurrentMemberId()
                        ,REQUEST_REMOVE_CLUB_MEMBER,ChangePostActivity.this);
                removeMember.execute("get");
            }
        });
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        if(respondId == REQUEST_CHANGE_MEMBERSHIP){
            StaticUserInterface.makeToast(ChangePostActivity.this,"User membership changed successfully ");
            finish();

        }
        else if(respondId == REQUEST_REMOVE_CLUB_MEMBER){

            StaticUserInterface.makeToast(ChangePostActivity.this,"User successfully removed from the club");
            finish();
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        if(respondId == REQUEST_CHANGE_MEMBERSHIP){

            StaticUserInterface.makeToast(ChangePostActivity.this,e.toString());
        }
        else if(respondId == REQUEST_REMOVE_CLUB_MEMBER){
            StaticUserInterface.makeToast(ChangePostActivity.this,e.toString());
        }

    }

    @Override
    public Context getContext() {
        return ChangePostActivity.this;
    }
}
