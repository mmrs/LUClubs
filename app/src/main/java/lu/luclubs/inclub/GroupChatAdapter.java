package lu.luclubs.inclub;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.RunnableFuture;

import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

import static lu.luclubs.inclub.MemberListAdapter.memberInfoArrayList;

/**
 * Created by Moshiur on 27-Sep-16.
 */

public class GroupChatAdapter extends BaseAdapter implements HTTPRequestIDs,HttpListener {

    Activity activity;
    ArrayList<MessageInfo> messageInfoArrayList = new ArrayList<>();


    public GroupChatAdapter(GroupChat groupChat) {
        this.activity = groupChat;
        getMessageRequestThread();
    }

    private void getMessageRequestThread() {

      new Thread(new Runnable() {
          @Override
          public void run() {
              while(StaticData.isGetMessageThreadRunnig()){
                  try {
                      synchronized(this){
                          Log.i("mmrs","get Message");
                          getMessageRequest();
                      }
                      Thread.sleep(5000);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
              }
          }
      }).start();
    }

    public void getMessageRequest() {

        HttpRequest addNewCr =  new HttpRequest(StaticData.getSocket()+"/api/clubs/messages/" + StaticData.getCurrentClubId(),
                REQUEST_GET_MESSAGES,GroupChatAdapter.this);
        addNewCr.execute("get");
    }

    @Override
    public int getCount() {
        return messageInfoArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.list_item_group_chat,parent,false);
        }

//        ImageView photo = (ImageView) convertView.findViewById(R.id.imageView_pending_request);
//        Picasso.with(activity)
//                .load(StaticData.getSocket()+((MemberInfo)memberInfoArrayList.get(position)).getPhoto())
//                .resize(70, 70)
//                .centerCrop()
//                .into(photo);
        ((TextView)convertView.findViewById(R.id.message_sender)).setText(messageInfoArrayList.get(position).getMessageSenderName() + "  ID:" +
                messageInfoArrayList.get(position).getSenderId());
        ((TextView)convertView.findViewById(R.id.message_body)).setText(messageInfoArrayList.get(position).getMessageBody());
        ((TextView)convertView.findViewById(R.id.message_datetime)).setText(messageInfoArrayList.get(position).getDateTime());
        return convertView;
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        JSONObject jsonObjectResponse;
        if(respondId == REQUEST_GET_MESSAGES){
            try{
                Log.i("mmrs","member list " +jsonRespond);
                jsonObjectResponse = new JSONObject(jsonRespond);
                JSONArray clubList = jsonObjectResponse.getJSONArray("messages");
                messageInfoArrayList.clear();
                for(int i=0;i<clubList.length();i++){
                    JSONObject info = clubList.getJSONObject(i);
                    MessageInfo memberInfo = new MessageInfo();
                    memberInfo.setMessageBody(info.getString("message_body"));
                    memberInfo.setDateTime(info.getString("created_at"));
                    JSONObject userInfo = info.getJSONObject("user");
                    memberInfo.setMessageSenderName(userInfo.getString("name"));
                    memberInfo.setSenderId(userInfo.getString("student_id"));
                    messageInfoArrayList.add(memberInfo);
                }
                notifyDataSetChanged();
            }catch (Exception e){
                Log.i("mmrs","exception in pending message parsing" + e.toString());

            }
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

    }

    @Override
    public Context getContext() {
        return activity;
    }
}
