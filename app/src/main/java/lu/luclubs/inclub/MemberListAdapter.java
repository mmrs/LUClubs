package lu.luclubs.inclub;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.clublist.ClubInfo;

import static android.R.attr.name;

/**
 * Created by Moshiur on 23-Sep-16.
 */

public class MemberListAdapter extends BaseAdapter {
    Activity activity;
    static ArrayList<MemberInfo> memberInfoArrayList;

    public MemberListAdapter(Activity groupMemberActivity) {
    this.activity = groupMemberActivity;
        if(StaticData.getMemberListType().equals("committee"))
          memberInfoArrayList = StaticData.getCommitteMember();
        else memberInfoArrayList = StaticData.getGeneraMember();
    }

    @Override
    public int getCount() {
      return memberInfoArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.list_item_member,parent,false);
        }
//        convertView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
//                builder1.setMessage("Sure To Remove this Club? All club related info and sections will be deleted permanently.");
//                builder1.setCancelable(true);
//                builder1.setPositiveButton("Confirm",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                positionToRemove = position;
//                                sendremoveClubRequest(position);
//
//                            }
//                        });
//                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog alert11 = builder1.create();
//                alert11.show();
//                alert11.setCanceledOnTouchOutside(false);
//                return false;
//            }
//        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!StaticData.getClubRole().equals("admin"))
                    return;
                StaticData.setCurrentMemberId(((MemberInfo)memberInfoArrayList.get(position)).getUserId());
                activity.startActivity(new Intent(activity, ChangePostActivity.class));

            }
        });
////            Log.i("getView",position + ": " + clubInfoArrayList.get(position).getName());

        ImageView photo = (ImageView) convertView.findViewById(R.id.imageView_member);
        Picasso.with(activity)
                .load(StaticData.getSocket()+((MemberInfo)memberInfoArrayList.get(position)).getPhoto())
                .resize(70, 70)
                .centerCrop()
                .into(photo);
        ((TextView)convertView.findViewById(R.id.member_name)).setText("Name: "+memberInfoArrayList.get(position).getName());
        ((TextView)convertView.findViewById(R.id.member_dept)).setText("Dept.: "+memberInfoArrayList.get(position).getDept());
//        ((TextView)convertView.findViewById(R.id.member_role)).setText("Membership: "+memberInfoArrayList.get(position).getRole());
        ((TextView)convertView.findViewById(R.id.member_roll)).setText("student ID: " + memberInfoArrayList.get(position).getStudentId());
        ((TextView)convertView.findViewById(R.id.member_batch)).setText("Session: " + memberInfoArrayList.get(position).getBatch());
        ((TextView)convertView.findViewById(R.id.member_role_clubs)).setText("Membership Info: \n" + memberInfoArrayList.get(position).getClubRoleInfo());
        ((TextView)convertView.findViewById(R.id.member_blood_group)).setText("Blood Group: \n" + memberInfoArrayList.get(position).getBloodGroup());

        return convertView;
    }
}
