package lu.luclubs.inclub;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import lu.luclubs.CreateNewEvent;
import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.administration.AdministrationOptionsActivity;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;

public class InClubActivity extends AppCompatActivity implements HttpListener,HTTPRequestIDs{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_club);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        RelativeLayout CM,GM,admin,message;
        CM = (RelativeLayout) findViewById(R.id.relative_layout_inClub_CM);
        GM = (RelativeLayout) findViewById(R.id.relative_layout_inClub_GM);
        admin = (RelativeLayout) findViewById(R.id.relative_layout_club_admin);
        message= (RelativeLayout) findViewById(R.id.relative_layout_clubwall);
        CM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.setMemberListType("committee");
                startActivity(new Intent(InClubActivity.this,GroupMemberActivity.class));
            }
        });
        GM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.setMemberListType("general");
                startActivity(new Intent(InClubActivity.this,GroupMemberActivity.class));
            }
        });
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InClubActivity.this,PendingRequestActivity.class));

            }
        });
        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InClubActivity.this,GroupChat.class));
            }
        });

        findViewById(R.id.relative_layout_create_event).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(InClubActivity.this,CreateNewEvent.class));
            }
        });

        sendGetMemberListRequest();

        if(!StaticData.getClubRole().equals("admin")){
           admin.setVisibility(View.GONE);
        }
        if(!StaticData.getClubRole().equals("admin")){
            findViewById(R.id.relative_layout_create_event).setVisibility(View.GONE);
        }
    }

    private void sendGetMemberListRequest() {

        HttpRequest addNewCr =  new HttpRequest(StaticData.getSocket()+"/api/clubs/members/" + StaticData.getCurrentClubId(),
                REQUEST_GET_MEMBER_LIST,InClubActivity.this);
        addNewCr.execute("get");
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        JSONObject jsonObjectResponse;
        if(respondId == REQUEST_GET_MEMBER_LIST){
            try{
                Log.i("mmrs","member list " +jsonRespond);
                jsonObjectResponse = new JSONObject(jsonRespond);
                JSONArray members = jsonObjectResponse.getJSONObject("club").getJSONArray("members");

                StaticData.allMemberInfoArrayList.clear();
                for(int i=0;i<members.length();i++){
                    String clubsNRoles = null;
                    clubsNRoles = jsonObjectResponse.getJSONObject("club").getString("name") + ": ";
                    JSONObject info = members.getJSONObject(i);
                    MemberInfo memberInfo = new MemberInfo();
                    memberInfo.setRole(info.getString("role"));
                    clubsNRoles += info.getString("role") + "\n";
                    memberInfo.setStatus(info.getString("status"));
                    JSONArray clubs = info.getJSONArray("clubs");
                    JSONObject userInfo = info.getJSONObject("user");
                    memberInfo.setUserId(userInfo.getString("id"));
                    memberInfo.setName(userInfo.getString("name"));
                    memberInfo.setStudentId(userInfo.getString("student_id"));
                    memberInfo.setDept(userInfo.getString("department"));
                    memberInfo.setBatch(userInfo.getString("batch"));
                    memberInfo.setPhoto(userInfo.getString("photo_url"));
                    memberInfo.setBloodGroup(userInfo.getString("blood_group"));
                    for(int j=0;j<clubs.length();j++){

                        clubsNRoles += clubs.getJSONObject(j).getJSONObject("club").getString("name") + ": ";
                        clubsNRoles += clubs.getJSONObject(j).getString("role") + "\n";
                    }
                    memberInfo.setClubRoleInfo(clubsNRoles);
                    StaticData.allMemberInfoArrayList.add(memberInfo);
                }
            }catch (Exception e){
                    Log.i("mmrs","exception in member parsing" + e);

            }
        }


    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {


    }

    @Override
    public Context getContext() {
        return InClubActivity.this;
    }
}
