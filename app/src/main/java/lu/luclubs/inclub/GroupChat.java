package lu.luclubs.inclub;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.VolleyError;

import java.util.List;

import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

import static lu.luclubs.inclub.MemberListAdapter.memberInfoArrayList;

public class GroupChat extends AppCompatActivity implements HttpListener,HTTPRequestIDs{

    GroupChatAdapter groupChatAdapter;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        StaticData.setGetMessageThreadRunnig(true);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        ListView listView = (ListView) findViewById(R.id.listview_messages);
        groupChatAdapter  = new GroupChatAdapter(GroupChat.this);
        listView.setAdapter(groupChatAdapter);


//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                groupChatAdapter.getMessageRequest();
//
//            }
//        });

        Button button = (Button) findViewById(R.id.button_send_message);
        editText = (EditText) findViewById(R.id.edittext_getmessage);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();

            }
        });

    }

    private void sendMessage() {

        if(editText.getText().toString().trim().isEmpty())
            return;
        HttpRequest logIn = new HttpRequest(StaticData.getSocket()+"/api/clubs/messages/new",REQUEST_SEND_MESSAGE,GroupChat.this);
        logIn.setParams(
                new KeyValue("club_id",StaticData.getCurrentClubId()),
                new KeyValue("user_id", StaticUserInfo.getId()),
                new KeyValue("message_body", editText.getText().toString())
        );
        logIn.execute("post");

    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        if(respondId == REQUEST_SEND_MESSAGE){
            Log.i("mmrs","message sent");
            editText.setText("");
        }

    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

    }

    @Override
    public Context getContext() {
        return GroupChat.this;
    }

    @Override
    protected void onStop() {
        StaticData.setGetMessageThreadRunnig(false);
        super.onStop();
    }
}
