package lu.luclubs;

import java.util.ArrayList;

import lu.luclubs.inclub.MemberInfo;

/**
 * Created by Siyam on 31-Aug-16.
 */
public class StaticData {

    public static String getSocket() {
        return socket;
    }

    public static void setSocket(String socket) {
        StaticData.socket = socket;

    //    StaticData.socket = "http://befaf384.ngrok.io:80";

    }

    static String socket;
    static String currentClubId;
    static boolean getMessageThreadRunnig;
    static String currentMemberId;
    private static String clubRole;

    public static String getCurrentMemberId() {
        return currentMemberId;
    }

    public static void setCurrentMemberId(String currentMemberId) {
        StaticData.currentMemberId = currentMemberId;
    }

    public static String getClubRole() {
        return clubRole;
    }

    public static void setClubRole(String clubRole) {
        StaticData.clubRole = clubRole;
    }

    public static boolean isGetMessageThreadRunnig() {
        return getMessageThreadRunnig;
    }

    public static void setGetMessageThreadRunnig(boolean getMessageThreadRunnig) {
        StaticData.getMessageThreadRunnig = getMessageThreadRunnig;
    }

    public static String getCurrentClubId() {
        return currentClubId;
    }

    public static void setCurrentClubId(String currentGroupPositionInListView) {
        StaticData.currentClubId = currentGroupPositionInListView;
    }

    public static ArrayList<MemberInfo> allMemberInfoArrayList = new ArrayList<>();
    public static ArrayList<MemberInfo> filteredMemberInfoArrayList = new ArrayList<>();
    public static String memberListType;

    public static String getMemberListType() {
        return memberListType;
    }

    public static void setMemberListType(String memberListType) {
        StaticData.memberListType = memberListType;
    }

    public static ArrayList<MemberInfo> getCommitteMember(){
        filteredMemberInfoArrayList.clear();
        for(int i=0;i<allMemberInfoArrayList.size();i++){
            if(!allMemberInfoArrayList.get(i).getRole().equals("user")){
                filteredMemberInfoArrayList.add(allMemberInfoArrayList.get(i));
            }
        }

        return filteredMemberInfoArrayList;
    }
    public static ArrayList<MemberInfo> getGeneraMember(){

        filteredMemberInfoArrayList.clear();
        for(int i=0;i<allMemberInfoArrayList.size();i++){
            if(allMemberInfoArrayList.get(i).getRole().equals("user") &&
                    allMemberInfoArrayList.get(i).getStatus().equals("approved")){
                filteredMemberInfoArrayList.add(allMemberInfoArrayList.get(i));
            }
        }
        return filteredMemberInfoArrayList;
    }

}
