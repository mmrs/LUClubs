package lu.luclubs.administration;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import lu.luclubs.CreateNewEvent;
import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInfo;

public class AdministrationOptionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administration_options);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        findViewById(R.id.relative_layout_admin_add_club).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdministrationOptionsActivity.this,AddNewClubActivity.class));
            }
        });
//        findViewById(R.id.relative_layout_create_event).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(AdministrationOptionsActivity.this,CreateNewEvent.class));
//            }
//        });

        if(!StaticUserInfo.getRole().equals("admin")){
            findViewById(R.id.relative_layout_admin_add_club).setVisibility(View.GONE);
        }

    }
}
