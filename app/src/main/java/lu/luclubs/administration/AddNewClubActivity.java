package lu.luclubs.administration;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import de.hdodenhof.circleimageview.CircleImageView;
import lu.luclubs.R;
import lu.luclubs.StaticData;
import lu.luclubs.StaticUserInterface;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpFileRequest;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class AddNewClubActivity extends AppCompatActivity implements HTTPRequestIDs,HttpListener{

    CircleImageView profileicture;
    final int SELECT_IMAGE = 233;
    String imageUri = null;
    Bitmap decoded;
    JSONObject jsonObjectResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_club);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        profileicture = (CircleImageView) findViewById(R.id.club_logo);
        Button update = (Button) findViewById(R.id.button_create_club);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageUri ==null || ((EditText) findViewById(R.id.club_name)).getText().toString().trim().isEmpty() ||
                        ((EditText) findViewById(R.id.club_admin_id)).getText().toString().trim().isEmpty())
                {
                    StaticUserInterface.makeToast(AddNewClubActivity.this,"All fields are mandatory");
                    return;
                }
                createClub();
            }
        });

        profileicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(pickIntent, "Select Image"), SELECT_IMAGE);

            }
        });

    }

    private void createClub() {

          if(imageUri !=null){
            File imageFile = new File(imageUri);
            long size = imageFile.length();
            size = size / 1024;
            if (size > 1024) {
                StaticUserInterface.makeToast(AddNewClubActivity.this,"selected logo size " +size + " KB." + "logo should be less than 1024KB.");
                return;
            }
        }

        StaticUserInterface.showProgressDialog(AddNewClubActivity.this, "Creating Club..");
        HttpFileRequest addClubRequest = new HttpFileRequest(StaticData.getSocket() + "/api/clubs/create",
                REQUEST_ADD_NEW_CLUB, AddNewClubActivity.this);
        try {
            addClubRequest.setFile("photo_url", imageUri);
            addClubRequest.setParams(
                    new KeyValue("name", ((EditText) findViewById(R.id.club_name)).getText().toString().trim()),
                    new KeyValue("admin_id", ((EditText) findViewById(R.id.club_admin_id)).getText().toString().trim()),
                    new KeyValue("description", ((EditText) findViewById(R.id.club_description)).getText().toString().trim())
            );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        addClubRequest.execute("post");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {

            Uri selectedImage = data.getData();
            imageUri = getPath(selectedImage);
            Bitmap bmp = BitmapFactory.decodeFile(imageUri);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            profileicture.setImageBitmap(decoded);
        }
        else {
            imageUri = null;
            profileicture.setImageResource(R.drawable.default_pro_pic);
        }
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public void respond(final String jsonRespond, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();
        Log.i("mmrs", "add clubtest : " + jsonRespond);
        try {
            if (respondId == REQUEST_ADD_NEW_CLUB) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StaticUserInterface.makeToast(AddNewClubActivity.this,"Club Added Successfully.");
                        finish();
                    }
                });
            }
        } catch (Exception e) {

            Log.i("mmrs", e.toString());
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();

        if (respondId == REQUEST_ADD_NEW_CLUB) {
            Log.i("mmrs", "create club volley error :" + e.toString());
            StaticUserInterface.alartDialog(AddNewClubActivity.this, e.toString());
        }
    }

    @Override
    public Context getContext() {
        return AddNewClubActivity.this;
    }

}
