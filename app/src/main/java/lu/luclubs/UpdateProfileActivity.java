package lu.luclubs;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import de.hdodenhof.circleimageview.CircleImageView;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpFileRequest;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class UpdateProfileActivity extends AppCompatActivity implements HttpListener, HTTPRequestIDs {

    CircleImageView profileicture;
    final int SELECT_IMAGE = 233;
    String imageUri = null;
    Bitmap decoded;
    JSONObject jsonObjectResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        profileicture = (CircleImageView) findViewById(R.id.update_photo);
        Button update = (Button) findViewById(R.id.update_profile);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!((EditText) findViewById(R.id.update_password)).getText().toString().trim().
                        equals(((EditText) findViewById(R.id.update_confirm_password)).getText().toString().trim())){
                    StaticUserInterface.makeToast(UpdateProfileActivity.this,"Password did not match");
                    return;
                }
                updateProfile();
            }
        });

        profileicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(pickIntent, "Select Image"), SELECT_IMAGE);

            }
        });
    }

    private void updateProfile() {

        if(imageUri !=null){
            File imageFile = new File(imageUri);
            long size = imageFile.length();
            size = size / 1024;
            if (size > 1024) {
                StaticUserInterface.makeToast(UpdateProfileActivity.this,"selected photo size " +size + " KB." + "photo should be less than 1024KB.");
                return;
            }
        }

        StaticUserInterface.showProgressDialog(UpdateProfileActivity.this, "Updating Profile..");
        HttpFileRequest updatePhotoRequest = new HttpFileRequest(StaticData.getSocket() + "/api/profileUpdate/" + StaticUserInfo.getId(),
                REQUEST_UPDATE_PROFILE, UpdateProfileActivity.this);
        try {
            if (imageUri!=null)
                updatePhotoRequest.setFile("photo_url", imageUri);
            updatePhotoRequest.setParams(
                    new KeyValue("name", ((EditText) findViewById(R.id.update_name)).getText().toString().trim().isEmpty() ? StaticUserInfo.getName() :
                            ((EditText) findViewById(R.id.update_name)).getText().toString().trim()),
                    new KeyValue("email", ((EditText) findViewById(R.id.update_email)).getText().toString().trim().isEmpty() ? StaticUserInfo.getEmail() :
                            ((EditText) findViewById(R.id.update_email)).getText().toString().trim()),
                    new KeyValue("mobile", ((EditText) findViewById(R.id.update_phone)).getText().toString().trim().isEmpty() ? StaticUserInfo.getMobile() :
                            ((EditText) findViewById(R.id.update_phone)).getText().toString().trim()),
                    new KeyValue("blood_group", ((EditText) findViewById(R.id.update_blood_group)).getText().toString().trim().isEmpty() ? StaticUserInfo.getMobile() :
                            ((EditText) findViewById(R.id.update_blood_group)).getText().toString().trim())

            );
            if (!((EditText) findViewById(R.id.update_password)).getText().toString().trim().isEmpty())
                updatePhotoRequest.setParams(new KeyValue("password", ((EditText) findViewById(R.id.update_password)).getText().toString()));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        updatePhotoRequest.execute("post");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {

            Uri selectedImage = data.getData();
            imageUri = getPath(selectedImage);
            Bitmap bmp = BitmapFactory.decodeFile(imageUri);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            profileicture.setImageBitmap(decoded);
        }
        else {
            imageUri = null;
            profileicture.setImageResource(R.drawable.default_pro_pic);
        }
    }

    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public void respond(final String jsonRespond, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();
        Log.i("mmrs", "test : " + jsonRespond);
        try {
            if (respondId == REQUEST_UPDATE_PROFILE) {
                jsonObjectResponse = new JSONObject(jsonRespond).getJSONObject("user");
                StaticUserInfo.setId(jsonObjectResponse.getString("id"));
                StaticUserInfo.setStudent_id(jsonObjectResponse.getString("student_id"));
                StaticUserInfo.setDepartment(jsonObjectResponse.getString("department"));
                StaticUserInfo.setRole(jsonObjectResponse.getString("role"));
                StaticUserInfo.setMobile(jsonObjectResponse.getString("mobile"));
                StaticUserInfo.setPhoto(StaticData.getSocket() + jsonObjectResponse.getString("photo_url"));
                StaticUserInfo.setEmail(jsonObjectResponse.getString("email"));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StaticUserInterface.makeToast(UpdateProfileActivity.this,"Profile Information Updated.");
                        finish();
                        Log.i("mmrs", "update profile :" + jsonRespond);
                    }
                });
            }
        } catch (Exception e) {

            Log.i("mmrs", e.toString());
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        StaticUserInterface.dismissProgressDialog();

        if (respondId == REQUEST_UPDATE_PROFILE) {
            Log.i("mmrs", "update profile volley error :" + e.toString());
            StaticUserInterface.alartDialog(UpdateProfileActivity.this, e.toString());
        }
    }

    @Override
    public Context getContext() {
        return UpdateProfileActivity.this;
    }

}
