package lu.luclubs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Siyam on 11-Aug-16.
 */
public class SharedPreferencesReadAndWrite{

    static SharedPreferences sharedPreferences;

    public static  String readItemFromSharedPreferences(Context context, String key){
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,null);
    }
    public static void writeItemToSharedPreferences(Context context,String key,String value){
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();

    }
    public static void clearSharedPreferencesData(Context context){

        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",null);
        editor.putString("password",null);
        editor.commit();
    }
}


