package lu.luclubs;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import lu.luclubs.R;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class ForgotPasswordActivity extends AppCompatActivity implements HTTPRequestIDs,HttpListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        findViewById(R.id.reset_password_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HttpRequest httpRequest = new HttpRequest(StaticData.getSocket()+"/api/password/recovery/"+
                        ((EditText)findViewById(R.id.forgot_email)).getText().toString(),
                        REQUEST_FORGOT_PASSWORD,ForgotPasswordActivity.this);
                httpRequest.execute("get");
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {

        StaticUserInterface.makeToast(ForgotPasswordActivity.this,"password sent to your email address");
        finish();
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        StaticUserInterface.makeToast(ForgotPasswordActivity.this,"opps! something went wrong");
    }

    @Override
    public Context getContext() {
        return ForgotPasswordActivity.this;
    }
}
