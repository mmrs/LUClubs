package lu.luclubs;

/**
 * Created by Siyam on 31-Aug-16.
 */
public class StaticUserInfo {

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        StaticUserInfo.id = id;
    }

    public static String getStudent_id() {
        return student_id;
    }

    public static void setStudent_id(String student_id) {
        StaticUserInfo.student_id = student_id;
    }

    public static String getDepartment() {
        return department;
    }

    public static void setDepartment(String department) {
        StaticUserInfo.department = department;
    }

    public static String getBatch() {
        return batch;
    }

    public static void setBatch(String batch) {
        StaticUserInfo.batch = batch;
    }

    public static String getRole() {
        return role;
    }

    public static void setRole(String role) {
        StaticUserInfo.role = role;
    }

    public static String getMobile() {
        return mobile;
    }

    public static void setMobile(String mobile) {
        StaticUserInfo.mobile = mobile;
    }

    public static String getPhoto() {
        return photo;
    }

    public static void setPhoto(String photo) {
        StaticUserInfo.photo = photo;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        StaticUserInfo.email = email;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        StaticUserInfo.name = name;
    }

    static String id,student_id,department,batch,role,mobile,photo,name,email;

}
