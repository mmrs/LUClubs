package lu.luclubs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class RegistrationActivity extends AppCompatActivity implements HttpListener,HTTPRequestIDs{

    AppCompatSpinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        findViewById(R.id.back_to_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!((EditText) findViewById(R.id.register_password)).getText().toString().trim().
                        equals(((EditText) findViewById(R.id.confirm_password)).getText().toString().trim())){
                    StaticUserInterface.makeToast(RegistrationActivity.this,"Password did not match");
                    return;
                }
                HttpRequest registerNewUserRequest = new HttpRequest(StaticData.getSocket()+"/api/register",
                        REQUEST_NEW_USER, RegistrationActivity.this);
                registerNewUserRequest.setParams(
                        new KeyValue("student_id", ((EditText) findViewById(R.id.register_student_id)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_student_id)).getText().toString().trim()),
                        new KeyValue("name", ((EditText) findViewById(R.id.register_name)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_name)).getText().toString().trim()),
                        new KeyValue("department", ((AppCompatSpinner) findViewById(R.id.register_spinner_departments)).getSelectedItem().toString()),
                        new KeyValue("batch", ((EditText) findViewById(R.id.register_batch)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_batch)).getText().toString().trim()),
                        new KeyValue("mobile", ((EditText) findViewById(R.id.register_phone)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_phone)).getText().toString().trim()),
                        new KeyValue("email", ((EditText) findViewById(R.id.register_email)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_email)).getText().toString().trim()),
                        new KeyValue("password", ((EditText) findViewById(R.id.register_password)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_password)).getText().toString().trim()),
                        new KeyValue("blood_group", ((EditText) findViewById(R.id.register_blood_group)).getText().toString().trim().isEmpty()?null:
                                ((EditText) findViewById(R.id.register_blood_group)).getText().toString().trim())

                );
                registerNewUserRequest.execute("post");
                StaticUserInterface.showProgressDialog(RegistrationActivity.this, "Registering Teacher.\nPlease Wait..");
            }
        });

        spinner = (AppCompatSpinner) findViewById(R.id.register_spinner_departments);
        // Spinner element
//        Spinner spinner = (Spinner) findViewById(R.id.spinner_departments);

        // Spinner click listener
//        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("CSE");
        categories.add("ARC");
        categories.add("ENGLISH");
        categories.add("EEE");
        categories.add("CIVIL");
        categories.add("LAW");
        categories.add("BBA");
        categories.add("Hotel Management");
        categories.add("Islamic Studies");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.department_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);



    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {
        if (respondId == REQUEST_NEW_USER) {
            try {
                Log.i("mmrs","register : " + jsonRespond);
                StaticUserInterface.dismissProgressDialog();
                JSONObject jsonObjectResponse = new JSONObject(jsonRespond);
                StaticUserInterface.makeToast(RegistrationActivity.this,"Registration Successful.");
                finish();

            } catch (Exception e) {
                StaticUserInterface.alartDialog(RegistrationActivity.this, e.toString());
            }
        }
    }
    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {
        if (respondId == REQUEST_NEW_USER) {
            StaticUserInterface.dismissProgressDialog();
            StaticUserInterface.alartDialog(RegistrationActivity.this, e.toString());
        }
    }
    @Override
    public Context getContext() {
        return RegistrationActivity.this;
    }

}
