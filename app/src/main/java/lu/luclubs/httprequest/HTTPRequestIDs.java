package lu.luclubs.httprequest;

/**
 * Created by Siyam on 13-Aug-16.
 */
public interface HTTPRequestIDs {

    int     REQUEST_NEW_USER = 200,
            REQUEST_LOG_IN = 201,
            REQUEST_UPDATE_PROFILE = 202,
            REQUEST_CLUB_LIST = 203,
            REQUEST_ADD_NEW_CLUB = 204,
            REQUEST_CREATE_NEW_EVENT = 205,
            REQUEST_GET_ALL_EVENTS = 206,
            REQUEST_REMOVE_CLUB_MEMBER = 207,
            REQUEST_FORGOT_PASSWORD = 208,
            REQUEST_DELETE_CLUB = 209,
            REQUEST_JOIN_CLUB = 210,
            REQUEST_GET_MEMBER_LIST = 211,
            REQUEST_PENDING_REQUEST_LIST = 212,
            REQUEST_PROCESS_REQUEST = 213,
            REQUEST_GET_MESSAGES = 214,
            REQUEST_SEND_MESSAGE = 215,
            REQUEST_CHANGE_MEMBERSHIP = 216;



}
