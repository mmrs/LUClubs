package lu.luclubs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import lu.luclubs.clublist.ClubListActivity;
import lu.luclubs.httprequest.HTTPRequestIDs;
import lu.luclubs.httprequest.HttpListener;
import lu.luclubs.httprequest.HttpRequest;
import lu.luclubs.httprequest.KeyValue;

public class CreateNewEvent extends AppCompatActivity implements HttpListener,HTTPRequestIDs{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        findViewById(R.id.button_create_event).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HttpRequest createEvent = new HttpRequest(StaticData.getSocket()+"/api/events/create",REQUEST_CREATE_NEW_EVENT,CreateNewEvent.this);
                createEvent.setParams(
                        new KeyValue("title",((EditText)findViewById(R.id.new_event_name)).getText().toString()),
                        new KeyValue("location",((EditText)findViewById(R.id.new_event_location)).getText().toString()),
                        new KeyValue("datetime",((EditText)findViewById(R.id.new_event_date_time)).getText().toString()),
                        new KeyValue("description",((EditText)findViewById(R.id.new_event_description)).getText().toString())

                );
                StaticUserInterface.showProgressDialog(CreateNewEvent.this,"please Wait..");
                createEvent.execute("post");
            }
        });
    }

    @Override
    public void respond(String jsonRespond, int respondId, HttpRequest parent) {
        try{
            if(respondId ==REQUEST_CREATE_NEW_EVENT){
                StaticUserInterface.dismissProgressDialog();
                StaticUserInterface.makeToast(CreateNewEvent.this,"Event Created");
                finish();
            }
        }
        catch (Exception e){
            StaticUserInterface.alartDialog(CreateNewEvent.this,e.toString());
        }
    }

    @Override
    public void errorRespond(VolleyError e, int respondId, HttpRequest parent) {

        if(respondId==REQUEST_CREATE_NEW_EVENT){
            StaticUserInterface.dismissProgressDialog();
            StaticUserInterface.alartDialog(CreateNewEvent.this,e.toString());
        }

    }

    @Override
    public Context getContext() {
        return CreateNewEvent.this;
    }

}
